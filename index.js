const fs = require('fs');
const path = require("path");
const config = require('./config.js')

const { basePath, baseUrl, scanPath, allowedExtensions } = config;

const getAllFiles = function(dirPath, arrayOfFiles) {
  files = fs.readdirSync(dirPath)

  arrayOfFiles = arrayOfFiles || []

  files.forEach(function(file) {
    let stats = fs.statSync(dirPath + "/" + file)
    if (stats.isDirectory()) {
      arrayOfFiles = getAllFiles(dirPath + "/" + file, arrayOfFiles)
    } else {
      if (allowedExtensions.includes(path.extname(file).toLowerCase()))
      arrayOfFiles.push({
        url: encodeURI(baseUrl + path.relative(
          basePath, path.join(
            dirPath, "/", file
          ))
        ), size:stats.size
      })
    }
  })

  return arrayOfFiles
}

try {
  const arrayOfFiles = {
    files: getAllFiles(scanPath)
  }
  console.log(JSON.stringify(arrayOfFiles))
} catch(e) {
  console.log(e)
}
